import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        auth_token: localStorage.getItem('token') || null,
    },
    getters: {
        TOKEN: state => {
            return state.auth_token;
        },
    },
    mutations: {
        SET_AUTH_TOKEN: (state, payload) => {
            state.auth_token = payload;
        },
    },
    actions: {
        SET_TOKEN: (context, token) =>  {
            context.commit('SET_AUTH_TOKEN', token);
            localStorage.setItem('token', token)
        },
    },
})
