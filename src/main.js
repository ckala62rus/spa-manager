window.axios = require('axios');

window.axios.defaults.headers.common['Authorization'] = `Bearer ${store.getters.TOKEN}`;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';
import VueAxios from 'vue-axios';

import {ClientTable, ServerTable, Event} from 'vue-tables-2';
Vue.use(ClientTable);
Vue.use(ServerTable);
Vue.use(Event);

import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
Vue.use(BootstrapVue);

import Toasted from 'vue-toasted';
Vue.use(Toasted, {duration: 2000});

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);

import vueKanban from 'vue-kanban';
Vue.use(vueKanban);

import VModal from 'vue-js-modal'
Vue.use(VModal)

Vue.config.productionTip = false;
Vue.use(VueAxios, axios);

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.TOKEN) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
