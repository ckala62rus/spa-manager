import Vue from 'vue';
import VueRouter from 'vue-router';

/******Мои компоненты******/
import Home from '../views/Home.vue';
import Auth from '../components/Auth/AuthIndex';
import Me from '../components/Auth/MeAuth';
import ForgotPassword from '../components/Auth/ForgotPassword';
import ResetPassword from '../components/Auth/ResetPassword';
import Registration from '../components/Auth/Registration';

import IndexRole from '../components/Role/IndexRole';
import Permissions from '../components/Role/Permissions';
import AddPermission from '../components/Role/AddPermission';

import Users from '../components/Users/Index';
import UserEdit from '../components/Users/UserEdit';

import ProjectAll from '../components/Project/ProjectAll';
import ProjectEdit from '../components/Project/ProjectEdit';
import AddProject from '../components/Project/AddProject';

import CardProjects from '../components/TaskManager/CardProjects';
import TaskBoard from '../components/TaskManager/TaskBoard';

Vue.use(VueRouter)

  const routes = [
    { path: '/', name: 'Home', component: Home },
    { path: '/about', name: 'About', component: () => import(/* webpackChunkName: "about" */ '../views/About.vue') },
    { path: '/me', name: 'me', meta: { requiresAuth: true }, component: Me },
    { path: '/login', name: 'login', component: Auth },
    { path: '/forgot', name: 'forgot', component: ForgotPassword },
    { path: '/reset/:token', name: 'reset', component: ResetPassword },
    { path: '/registration', name: 'registration', component: Registration },

    /*Роли*/
    { path: '/role', name: 'role', meta: { requiresAuth: true }, component: IndexRole },

    /*Разрешения*/
    { path: '/permissions', name: 'permissions', meta: { requiresAuth: true }, component: Permissions },
    { path: '/add-permission', name: 'add-permission', meta: { requiresAuth: true }, component: AddPermission },

    /*Пользователи*/
    { path: '/users', name: 'users', meta: { requiresAuth: true }, component: Users },
    { path: '/users/:id', name: 'user-edit', meta: { requiresAuth: true }, component: UserEdit },

    /*Проекты*/
    { path: '/projects', name: 'projects', meta: { requiresAuth: true }, component: ProjectAll },
    { path: '/projects/create', name: 'create-project', meta: { requiresAuth: true }, component: AddProject },
    { path: '/projects/:id', name: 'project-edit', meta: { requiresAuth: true }, component: ProjectEdit },

    /*Менеджер проектов*/
    { path: '/projects-cards', name: 'projects-cards', meta: { requiresAuth: true }, component: CardProjects },
    { path: '/task-board/:id', name: 'task-board', meta: { requiresAuth: true }, component: TaskBoard },
  ]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
